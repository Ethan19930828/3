//
//  ViewController.swift
//  RanDom
//
//  Created by zwx on 27/11/2014.
//  Copyright (c) 2014 zwx. All rights reserved.
//

import UIKit
import Social


class ViewController: UIViewController {

    @IBOutlet weak var randomNumber: UILabel!

    /*
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet var left: UISwipeGestureRecognizer!
    @IBOutlet var right: UISwipeGestureRecognizer!
*/
    
    
    /*@IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        println("swipeLeft")
        self.pageIndicator.currentPage++
        println("page: \(self.pageIndicator.currentPage)")
        //self.randomNumber.text = "page: \(self.pageIndicator.currentPage + 1)"
    }
    
    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        println("swipeRight")
        self.pageIndicator.currentPage--
        println("page: \(self.pageIndicator.currentPage)")
        //self.randomNumber.text = "page: \(self.pageIndicator.currentPage - 1)"
    }*/
    
    
    @IBAction func generate(sender: UIButton) {
        println("get a quote!")
        let quoteUrl = "http://www.iheartquotes.com/api/v1/random?format=json4&max_characters=160&width=225&height=165"
        //http://www.iheartquotes.com/api/v1/random?format=json4
        //http://www.iheartquotes.com/api/v1/random?source=einstein&format=json
        //http://www.iheartquotes.com/api/v1/random?source=gandhi&format=json
        //http://www.iheartquotes.com/api/v1/random?source=shakespeare&format=json
        
        
        let session = NSURLSession.sharedSession()
        session.dataTaskWithURL(NSURL(string: quoteUrl)!, completionHandler: {
            (data, response, error) in
            if error != nil {
                println("Error retrieveing quote: \(error.localizedDescription)")
                return
            }
            
            var errorJSON: NSError?
            let quoteAny: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: .allZeros, error: &errorJSON)
            if errorJSON != nil {
                println("Error parsing quote: \(errorJSON!.localizedDescription)")
                return
            }
            
            if let quoteJSON = quoteAny as? [String: AnyObject] {
                
                let quote = quoteJSON["quote"] as String
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.randomNumber.text = quote
                })
            }
        }).resume()
    }
    
    @IBAction func shareToFacebook() {
        
        var shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        shareToFacebook.setInitialText(self.randomNumber.text)
        
        self.presentViewController(shareToFacebook, animated: true, completion: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /*
        self.view.addGestureRecognizer(self.right)
        self.view.addGestureRecognizer(self.left)
        self.pageIndicator.numberOfPages = 4
        self.pageIndicator.currentPage = 0
*/
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

